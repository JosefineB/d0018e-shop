<?php 
	require_once("util/util.php");
	startSession();
 ?>
<!-- The header should display the searchbar and other navigational buttons/links so we can navigate around the site, possibly make the title logo reset back to the hompage

TODO: should probably be split up into several different parts
-->
<head>
	<link rel="stylesheet" type="text/css" href="homepage.css">
	<script defer src = "fontawesome-free-5.11.2-web\fontawesome-free-5.11.2-web\js\all.js"></script>
	<title>theBookShop</title>
</head>
<body>
	<a class = "title" href="index.php" >theBookShop</a>
	<?php 
		require_once('searchbar.php');
		require('util/userbutton.php');
		printUserButton();
	?>
	<div class="cart">
		<form action="cart.php" method="GET"> 
			<button type="submit" class="cartButton">
				<i class="fas fa-shopping-cart"></i>
			</button>
		</form>
	</div>
	<?php require_once('sidebar.php') ?>


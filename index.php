<?php 
  require("util/util.php"); //Not using require_once just so we don't have to restart browser when reseting cookies
  setID();
?>
<!-- The hompage should display the header and lists some books from the database-->
<!DOCTYPE html>
<html>
	<?php 
    require('header.php');
    require_once("util/search.php"); ?>
	<div class="main">
    <?php 
      searchFor(""); ?>
	</div>
</body>
</html>

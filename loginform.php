<?php require_once('util/startSession.php'); ?>
<!DOCTYPE html>
<html>
<body>
	<?php require('header.php'); ?>
	<div class='main'>
		<?php 
			if (isset($_SESSION['loginerror'])) {
  			echo $_SESSION['loginerror'];
  			unset($_SESSION['loginerror']);
			}
		 ?>
		<div class='loginForm'>
			<form action="util/login.php" method="POST">
				<h3>User credentials</h3>
					<label for='uname'>Username:</label><br>
					<input type='text' id='uname' name='uname'/><br>
					<label for='pswrd'>Password:</label><br>
					<input type='password' id='pswrd' name='pswrd'/><br>
					<button type ='submit' class='loginButton'>Login</button>
			</form>
			<h3>Don't have an account?</h3>
			<form action="registerform.php" method="GET">
				<button type ='submit' class='accountButton'>Create account</button>
			</form>
		</div>
	</div>
</body>
</html>


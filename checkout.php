<!DOCTYPE html>
<html>
<body>
	<?php 
	require('header.php');
	require_once('util/checkout.php');
	require_once('util/connection.php');
	require_once('util/util.php');
	 ?>
	<div class='main'>
		<?php
			if(userIsLoggedIn()){
				placeOrder($_SESSION['user_id']);
			} 
			else{
				placeOrder($_COOKIE['custID']);
			} 
		?> 
	</div>
</body>
</html>

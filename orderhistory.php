<!DOCTYPE html>
<html>
<body>
	<?php 
	require('header.php');
	require('util/checkout.php');
	require('util/orderhistory.php');
	require_once('util/connection.php');
	require_once('util/util.php');
	 ?>
	<div class='main'>
		<?php 
		$db = connect(); 
		$order = getOrderByID($_GET['orderID'], $db);
		if(sizeof($order) == 0){ //Just for testing
			echo" No order available";
		}else{
			printOrder($order, $db, true, "Your order: " . $_GET['orderID']); 
		}
		
		?>
	</div>
</body>
</html>
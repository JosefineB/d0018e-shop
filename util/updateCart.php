<?php 
	
	session_start();
	require('connection.php');


	function updateQty($bookID,$qty, $userID){
		$db = connect(); 
		if($qty > 0){ 
			$stm = $db->prepare("UPDATE cartItems SET quantity = ? WHERE custID = ? AND bookID = ?");
			$stm->execute([$qty, $userID, $bookID]); 
		}else{
			$stm = $db->prepare("DELETE FROM cartItems WHERE custID = ? AND bookID = ?");
			$stm->execute([$userID, $bookID]); 
		} 
	}


	if(isset($_SESSION['user_id'])){
		updateQty($_GET['bookID'], $_GET['quantity'], $_SESSION['user_id']);
	}else{
		updateQty($_GET['bookID'], $_GET['quantity'], $_COOKIE['custID']);
	}
	header("Location: ../cart.php");
	exit;


 ?>

<?php

require_once('connection.php');
require_once('arrays.php');

/* DB schema constants (table and column names) */
const BOOKS_TABLE =               "Books";
const BOOKS_ID_COL =              "bookID";
const BOOKS_TITLE_COL =           "title";
const BOOKS_IMG_COL =             "cover";
const BOOKS_PRICE_COL =           "price";
const BOOKS_GENRE_COL =           "genre";
const BOOKS_STOCK_COL =           "stock";
const ITEMS_TABLE =               "cartItems";
const ITEMS_USER_COL =            "custID";
const ITEMS_BOOK_COL =            "bookID";
const ITEMS_QUANTITY_COL =        "quantity";
const ORDERS_TABLE =              "Orders";
const ORDERS_ID_COL =             "orderID";
const ORDERS_USER_COL =           "custID";
const ORDERS_DATE_COL =           "orderDate";
const CUSTOMERS_TABLE =           "Customers";
const CUSTOMERS_ID_COL =          "custID";
const CUSTOMERS_FIRST_NAME_COL =  "fName";
const CUSTOMERS_LAST_NAME_COL =   "LName";
const CUSTOMERS_ADDR_COL =        "address";
const CUSTOMERS_EMAIL_COL =       "email";
const PASSWORDS_TABLE =           "Passwords";
const PASSWORDS_NAME_COL =        "uName";
const PASSWORDS_USER_ID_COL =     "custID";
const PASSWORDS_PASSWORD_COL =    "pswrd";
const REVIEWS_TABLE =             "Reviews";
const REVIEWS_BOOK_COL =          "bookID";
const REVIEWS_USER_COL =          "reviewer";
const REVIEWS_REVIEW_COL =        "comment";
const REVIEWS_RATING_COL =        "rating";
const AUTHORS_TABLE =             "Authors";
const AUTHORS_ID_COL =            "authorID";
const AUTHORS_NAME_COL =          "fullname";
const BOOKAUTHORS_TABLE =         "BookAuthors";
const BOOKAUTHORS_BOOK_COL =      "bookID";
const BOOKAUTHORS_AUTHOR_COL =    "authorID";

// All query functions in this file use this DB connection.
$db = connect();

function fetchAssoc($q, $mappings = array()) {
  global $db;
  $s = $db->prepare($q);
  $s->execute($mappings);
  return $s->fetch(PDO::FETCH_ASSOC);
}

function fetchAll($q, $mappings = array()) {
  global $db;
  $s = $db->prepare($q);
  $s->execute($mappings);
  return $s->fetchAll();
}

function getBookById($id) {
  $table = BOOKS_TABLE;
  $book = BOOKS_ID_COL;
  $q = "SELECT * FROM $table WHERE $book = ?";
  return fetchAssoc($q, [$id]);
}

/* Takes a book (as an associative array) and adds average rating and authors to it. */
function getCompleteBookInfo($book) {
  $bookId = $book[BOOKS_ID_COL];
  $avgRating = getAverageRating($bookId);
  if ($avgRating == '') {
    $avgRating = 'No rating';
  } else {
    $avgRating = sprintf('%.1f', $avgRating);
  }
  $authors = getBookAuthorsAsString($bookId);
  $info = ['avgRating' => $avgRating, 'authors' => $authors];
  return array_merge($book, $info);
}

function getCompleteBookInfoById($bookId) {
  $book = getBookById($bookId);
  return getCompleteBookInfo($book);
}

function getBookAuthorsAsList($bookID) {
  global $db;
  $db->beginTransaction();
  $stm = $db->prepare("DROP TABLE IF EXISTS Temp");
  $stm->execute();
  $stm = $db->prepare("CREATE TEMPORARY TABLE Temp (SELECT authorID FROM BookAuthors AS authorID WHERE bookID = ?)");
  $stm->execute([$bookID]);
  $authors = fetchAll("SELECT Authors.fullname FROM Authors INNER JOIN Temp ON Authors.authorID=Temp.authorID");
  $db->commit();
  return $authors;
}

function getBookAuthorsAsString($bookID) {
  $authors = getBookAuthorsAsList($bookID);
  return implode(', ', array_map(function($author) { return $author[AUTHORS_NAME_COL]; }, $authors));
}

/*
 * Make an associative array of bindings to use when executing a prepared
 * statement. Takes an associative array mapping column names to values and
 * prepends a colon (:) to the keys (columns).
 * Maps ['col1' => val1, 'col2' => val2, ..., 'coln' => valn]
 * to   [':col1' => val1, ':col2' => val2, ..., ':coln' => valn].
 */
function getPlaceHoldersToValuesArray($values) {
  return array_map_keys($values, function ($s) { return ":$s"; });
}

/*
 * Returns the ON DUPLICATE KEY clause necessary to replace a row.
 * In other words, if an INSERT would fail because the key exists,
 * the row that was being inserted simply replaces the existing one.
 * Note: This is MySQL-only!
 *
 * Takes an associative array where the keys are column names.
 * (Only the keys (column names) are relevant. The values are ignored.)
 * Uses assignment: 'col = col' for each column.
 */
function getOnDupReplaceClause($values) {
  $f = function ($col) { return "$col = :$col"; };
  $assignments = join(', ', array_map($f, array_keys($values)));
  return " ON DUPLICATE KEY UPDATE $assignments";
}

function joinKeys($values, $sep = ', ') {
  return join($sep, array_keys($values));
}

/* Take (table, [col1 => val1, col2 => val2, coln => valn]) and produce
 * INSERT INTO Table (col1, col2, coln) VALUES (:col1, :col2, :coln)
 * or
 * INSERT INTO Table (col1, col2, coln) VALUES (:col1, :col2, :coln) ON DUPLICATE KEY UPDATE col1 = :_col1, col2 = :_col2, coln = :_coln
 * if the $replace parameter is true.
 * Returns the SQL query string.
 */
function getInsertIntoQuery($table, $values, $mappings, $replace = false) {
  $cols = joinKeys($values);
  $vals = joinKeys($mappings);
  $q = "INSERT INTO $table ($cols) VALUES ($vals)";
  if ($replace) {
    $q .= getOnDupReplaceClause($values);
  }
  return $q;
}

/* Take (table, [col1 => val1, col2 => val2, coln => valn]) and insert a new row into the table (using a prepared statement).
 * If the optional $replace parameter is true, the row is replaced with the new row.
 * Returns the ID of the inserted/modified row or false if the query fails.
 */
function insertInto($table, $values = array(), $replace = false) {
  global $db;
  $mappings = getPlaceHoldersToValuesArray($values);
  $q = getInsertIntoQuery($table, $values, $mappings, $replace);
  $stm = $db->prepare($q);
  $result = $stm->execute($mappings);
  if ($result) {
    return $db->lastInsertId();
  }
  return false;
}

// Deprecated - unused.
function getUpdateQuery($table, $values) {
  $f = function ($col) { return "$col = ':$col'"; };
  $pairs = array_map($f, array_keys($values));
  $mappings = getPlaceHoldersToValuesArray($values);
  return "UPDATE $table SET " . implode(", ", $pairs);
}

function newCustomer() {
  global $db;
  $db->beginTransaction();
  $id = insertInto(CUSTOMERS_TABLE);
  $db->commit();
  return $id;
}

function getAverageRating($bookId) {
  $table = REVIEWS_TABLE;
  $rating = REVIEWS_RATING_COL;
  $book = REVIEWS_BOOK_COL;
  return fetchAssoc("SELECT AVG($rating) AS avgRating FROM $table WHERE $book = ?", [$bookId])['avgRating'];
}

function getBookReviews($bookId) {
  $table = REVIEWS_TABLE;
  $rating = REVIEWS_RATING_COL;
  $book = REVIEWS_BOOK_COL;
  return fetchAll("SELECT * FROM $table WHERE $book = ?", [$bookId]);
}

function getBookReview($bookId, $userId) {
  $table = REVIEWS_TABLE;
  $text = REVIEWS_REVIEW_COL;
  $rating = REVIEWS_RATING_COL;
  $user = REVIEWS_USER_COL;
  $book = REVIEWS_BOOK_COL;
  $q = "SELECT $text, $rating FROM $table WHERE $user = ? AND $book = ?";
  return fetchAssoc($q, [$userId, $bookId]);
}

function updateBookReview($values) {
  $table = REVIEWS_TABLE;
  return insertInto($table, $values, true);
}

?>

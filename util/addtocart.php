<?php 
	
	session_start();
	require('connection.php');

	function addToCart($bookID, $userID){
		$db = connect(); 
		$stm = $db->prepare("INSERT INTO cartItems(custID, bookID, quantity) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE quantity = quantity + 1");
		$stm->execute([$userID, $bookID, 1]);
	}

	if(isset($_SESSION['user_id'])){
		addToCart($_GET['bookID'], $_SESSION['user_id']);
	}else{
		addToCart($_GET['bookID'], $_COOKIE['custID']);
	}
	header("Location: ../cart.php");
	exit;


 ?>

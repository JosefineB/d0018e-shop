<?php

require_once('db.php');

const ID_SESSION = 'user_id';
const ID_COOKIE  = 'custID';

function startSession() {
  if (session_status() == PHP_SESSION_NONE) {
    session_start();
  }
}

function restartSession() {
  session_destroy();
  session_start();
}

function userIsLoggedIn() {
  return isset($_SESSION[ID_SESSION]);
}

function idCookieIsSet() {
  return isset($_COOKIE[ID_COOKIE]);
}

function setIdCookie($id) {
  if (!idCookieIsSet()) {
    setcookie(ID_COOKIE, $id, strtotime('+30 days'));
  }
}

function setID() {
  if (!idCookieIsSet()) {
    setIdCookie(newCustomer());
  }
}

// Return the appropriate user ID depending on whether the user is logged in.
function getUserId() {
	if (userIsLoggedIn()) {
		return $_SESSION[ID_SESSION];
	} else {
		return $_COOKIE[ID_COOKIE];
	}
}

?>

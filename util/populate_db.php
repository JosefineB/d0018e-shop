<?php

require_once('db.php');

// The format of the expected CSV file is
// ID, Title, "Author1[, Author2, ..., AuthorN]", Genre, URL, Stock, Price
// For example:
//1,Databasteknik,"Thomas Padron-McCarthy, Tore Risch",Data/It,https://s1.adlibris.com/images/43792868/databasteknik.jpg,0,509

// Fields with multiple words (such as authors and most titles) are quoted.
// Need to remove the quotes (after the field has been extracted!)
function removeQuotes($field) {
  $tr = array('"' => '');
  return strtr($field, $tr);
}

function verboseRunQuery($db, $q) {
  echo "running query: $q\n";
  if ($db->query($q)) {
    printf("  OK\n");
  } else {
    $err = $db->errorInfo();
    $msg = $err[2];
    printf("  FAILED: %s\n", $msg);
  }
}

function dropTable($db, $table) {
  $q = "DROP TABLE $table";
  verboseRunQuery($db, $q);
}

function dropForeignKey($db, $table, $fk) {
  $q = "ALTER TABLE $table DROP FOREIGN KEY $fk";
  verboseRunQuery($db, $q);
}

function dropTables($db) {
  echo "dropping tables...\n";
  //dropForeignKey($db, BOOKAUTHORS_TABLE, BOOKAUTHORS_BOOK_COL);
  //dropForeignKey($db, BOOKAUTHORS_TABLE, BOOKAUTHORS_AUTHOR_COL);
  //dropForeignKey($db, BOOKS_TABLE, BOOKS_ID_COL);
  dropTable($db, BOOKAUTHORS_TABLE);
  dropTable($db, BOOKS_TABLE);
  dropTable($db, AUTHORS_TABLE);
}

function createBooksTable() {
  $table = BOOKS_TABLE;
  $id = BOOKS_ID_COL;
  $title = BOOKS_TITLE_COL;
  $genre = BOOKS_GENRE_COL;
  $cover = BOOKS_IMG_COL;
  $stock = BOOKS_STOCK_COL;
  $price = BOOKS_PRICE_COL;
  $varchar = 'varchar(255)';
  $q = <<<SQL
  CREATE TABLE $table (
    $id int(5) unsigned PRIMARY KEY AUTO_INCREMENT,
    $title varchar(300),
    $genre varchar(30),
    $cover varchar(300),
    $stock int(3),
    $price int(4)
  )
SQL;
  return $q;
}

function createAuthorsTable() {
  $table = AUTHORS_TABLE;
  $id = AUTHORS_ID_COL;
  $name = AUTHORS_NAME_COL;
  $varchar = 'varchar(255)';
  $q = <<<SQL
  CREATE TABLE $table (
    $id int(6) unsigned PRIMARY KEY AUTO_INCREMENT,
    $name varchar(100)
  )
SQL;
  return $q;
}

function createBookAuthorsTable() {
  $table = BOOKAUTHORS_TABLE;
  $bookId = BOOKAUTHORS_BOOK_COL;
  $authorId = BOOKAUTHORS_AUTHOR_COL;
  $booksIdCol = BOOKS_ID_COL;
  $authorsIdCol = AUTHORS_ID_COL;
  $books = BOOKS_TABLE;
  $authors = AUTHORS_TABLE;
  $q = <<<SQL
  CREATE TABLE $table (
    $bookId int(5) unsigned,
    $authorId int(6) unsigned,
    FOREIGN KEY ($bookId) REFERENCES $books($booksIdCol),
    FOREIGN KEY ($authorId) REFERENCES $authors($authorsIdCol),
    PRIMARY KEY ($bookId, $authorId)
  )
SQL;
  return $q;
}

function createTables($db) {
  echo "creating tables...\n";
  verboseRunQuery($db, createBooksTable());
  verboseRunQuery($db, createAuthorsTable());
  verboseRunQuery($db, createBookAuthorsTable());
}

function getAuthorId($db, $authorName) {
  $table = AUTHORS_TABLE;
  $id = AUTHORS_ID_COL;
  $name = AUTHORS_NAME_COL;
  $result = fetchAssoc("SELECT $id FROM $table WHERE $name = '$authorName'");
  if ($result) {
    return $result[AUTHORS_ID_COL];
  } else {
    return false;
  }
}

// Takes an array of names, adds each author to the databse, and
// returns an array with the corresponding IDs.
function addAuthors($db, $authorNames) {
  $ids = array();
  $table = AUTHORS_TABLE;
  foreach ($authorNames as $author) {
    $id = getAuthorId($db, $author);
    if ($id) {
      echo "the author '$author' already exists with ID $id\n";
    } else {
      $values = array(AUTHORS_NAME_COL => $author);
      verboseRunQuery($db, insertInto($table, $values));
      $id = $db->lastInsertId();
      echo "the author was added with id = $id\n";
    }
    array_push($ids, $id);
  }
  return $ids;
}

// Adds a book to the database and returns its ID.
function addBook($db, $title, $genre, $url, $stock, $price) {
  $table = BOOKS_TABLE;
  $values = array(
    BOOKS_TITLE_COL => $title,
    BOOKS_GENRE_COL => $genre,
    BOOKS_IMG_COL => $url,
    BOOKS_STOCK_COL => $stock,
    BOOKS_PRICE_COL => $price,
  );
  verboseRunQuery($db, insertInto($table, $values));
  $id = $db->lastInsertId();
  echo "the book was added with id = $id\n";
  return $id;
}

// Takes a book ID and an array of IDs of the authors.
// Adds each book-author relationship to the BookAuthors table.
function addBookAuthors($db, $bookId, $authorIds) {
  $table = BOOKAUTHORS_TABLE;
  foreach ($authorIds as $authorId) {
    $values = array(BOOKAUTHORS_BOOK_COL => $bookId, BOOKAUTHORS_AUTHOR_COL => $authorId);
    verboseRunQuery($db, insertInto($table, $values));
  }
}

// Takes the name of a CSV file.
// Clear the database (drop and recreate tables).
// Then add all the books and the authors of those books to the database.
function populateDB($csvFileName) {
  global $db;
  $csvFile = fopen($csvFileName, 'r');
  dropTables($db);
  createTables($db);
  while ($csvFields = fgetcsv($csvFile)) {
    echo "processing CSV record...\n";
    $title = removeQuotes($csvFields[1]);
    $genre = $csvFields[3];
    $url = $csvFields[4];
    $stock = $csvFields[5];
    $price = $csvFields[6];
    $authorNames = explode(", ", removeQuotes($csvFields[2]));
    $authors = addAuthors($db, $authorNames);
    $id = addBook($db, $title, $genre, $url, $stock, $price);
    addBookAuthors($db, $id, $authors);
  }
}

populateDB('booksfromdb.csv');

?>

<?php 
	
	//session_start();
  require("connection.php");

	function registerUser($username, $password, $firstname, $lastname, $address, $email, $ID) {
		$db = connect();
		if (isUsernameValid($username, $db)) {
			registrationQuery($username, $password, $firstname, $lastname, $address, $email, $ID, $db);
			header("Location: ../loginform.php"); // TODO: should probably login the user instead.
			exit;
		} else {
			$_SESSION['registererror'] = 'The username ' . $username .' is already taken, please choose a new one. <br><br>';
			header("Location: ../registerform.php");
			exit;
		}
	}

	function isUsernameValid($username, $db) {
			$stm = $db->prepare("SELECT uName AS uName FROM Passwords WHERE uName = :uname");
			$stm->execute([':uname' => $username]);
			$customer = $stm->fetch(PDO::FETCH_ASSOC);
			if (sizeof($customer)> 0 && $customer != false) {
				return false;
			}
			else {
				return true;
			}
	}

	function registrationQuery($username, $password, $firstname, $lastname, $address, $email, $ID, $db) {
		$custstm = $db->prepare("UPDATE Customers SET fName = ?, LName  = ?, address  = ?, email  = ? WHERE custID= ?");
		$custstm->execute([$firstname, $lastname, $address, $email, $ID]);
		$passstm = $db->prepare("INSERT INTO Passwords (uName, pswrd, custID) VALUES (?, ?, ?)");
		$passstm->execute([$username, $password, $ID]);
	}

	registerUser($_POST['uname'], $_POST['pswrd'], $_POST['fname'], $_POST['lname'], $_POST['addr'], $_POST['email'], $_COOKIE['custID']);

 ?>

<?php 
	
	function createOrder($custID, $db){
		$stm = $db->prepare("INSERT INTO Orders (custID, orderDate, state) VALUES (?, ?, ?)");
		$stm->execute([$custID, date('Y-m-d', time()), 1]);
	}

	function isLoggedIn(){
		if(isset($_SESSION['user_id'])){
			return true;
		}else{
			return false;
		}
	}

	function getOrderID($custID, $db){
		$stm = $db->prepare("SELECT MAX(orderID) AS orderID FROM Orders WHERE custID = :id");
		$stm->execute([':id' => $custID]);
		$order = $stm->fetch(\PDO::FETCH_ASSOC);
		return $order['orderID'];
	}

	function getCart($custID, $db){
		if(isLoggedIn()){
			$stm = $db->prepare("SELECT * FROM cartItems WHERE custID = :sessid OR custID = :cookie");
			$stm->execute([':sessid' => $custID, ':cookie' => $_COOKIE['custID']]);
		}
		else{
			$stm = $db->prepare("SELECT * FROM cartItems WHERE custID = :cookie");
			$stm->execute([':cookie' => $custID]);
		}
		$cart = $stm->fetchAll();
		return $cart;
	}

	function placeOrder($custID){
		$db = connect();
		createOrder($custID, $db);
		$orderID = getOrderID($custID, $db);
		setCustomerInfo($custID, $db);
		$cart = getCart($custID, $db);
		$db->beginTransaction();
		transferItems($cart, $custID, $orderID, $db);
		emptyCart($custID, $db);//this could possibly be called inside transferItems, making the transaction time a bit shorter
		$db->commit();
	}
	
	function emptyCart($custID, $db){
		if(isLoggedIn()){
			$stm = $db->prepare("DELETE FROM cartItems WHERE custID = :sessid OR custID = :cookie");
			$stm->execute([':sessid' => $custID, ':cookie' => $_COOKIE['custID']]);
		}else{
			$stm = $db->prepare("DELETE FROM cartItems WHERE custID = :cookie");
			$stm->execute([':cookie' => $custID]);
		}
	}

	function transferItems($cart, $custID, $orderID, $db){
		$instock = array();
		$outofstock = array();
		$i = 0;
		$j = 0;
		foreach ($cart as $cartItem) {
			$stm = $db->prepare("SELECT * FROM Books WHERE bookID = :bID");
			$stm->execute([':bID' => $cartItem['bookID']]);
			$selectedBook = $stm->fetch(\PDO::FETCH_ASSOC);
			if($selectedBook['stock'] >= $cartItem['quantity']){
				$orderstm = $db->prepare("INSERT INTO OrderItems (orderID, bookID, quantity, price) VALUES (?, ?, ?, ?)");
				$orderstm->execute([$orderID ,$cartItem['bookID'], $cartItem['quantity'], $selectedBook['price']]);
				$stockstm = $db->prepare("UPDATE Books SET stock= stock - :qty WHERE bookID = :bID");
				$stockstm->execute([':qty'=> $cartItem['quantity'], ':bID' => $cartItem['bookID']]);
				$instock[$i] = $cartItem;
				$i++;
			}else{
				$outofstock[$j] = $cartItem;
				$j++;
			}	
		}
		orderSumLayout($instock, $outofstock, $db);
	}

	function orderSumLayout($instock, $outofstock, $db){
		if(sizeof($instock) == 0 && sizeof($outofstock) > 0){
			printOrder($outofstock, $db, false, 'These items were not available: ');
		}else if(sizeof($outofstock)== 0 && sizeof($instock) > 0){
			printOrder($instock, $db, true, 'Your order: ');
		}else{
			printOrder($instock, $db, true, 'Your order: ');
			printOrder($outofstock, $db, false, 'These items were not available: ');
		}
	}

	function setCustomerInfo($custID, $db){ 
		$stm = $db->prepare("UPDATE Customers SET fName = :fname, LName  = :lname, address  = :addr, email  = :email WHERE custID= :cID");
		$stm->execute([':fname' => $_GET['fname'], ':lname' => $_GET['lname'], ':addr' => $_GET['addr'], ':email' => $_GET['email'], ':cID' => $custID]);
	}


	function printOrder($cart, $db, $isInstock, $msg){
		echo"	<table id='ordersum'>";
		echo"		<caption>$msg</caption>";
		echo" 	<thead>";
		echo"    <tr>";
		echo"    	<th class='photoCol'>Photo</th>";
		echo"      <th class='qtyCol'>Qty</th>";
		echo"      <th class='titleCol'>Product</th>";
		echo"      <th class='priceCol'>Price</th>";
		echo"    </tr>";
		echo"  </thead>";
		echo"  <tbody>";
		$sum=0;
		foreach ($cart as $cartItem) {
			$stm = $db->prepare("SELECT * FROM Books WHERE bookID = :bID");
			$stm->execute([':bID' => $cartItem['bookID']]);
			$book = $stm->fetch(PDO::FETCH_ASSOC);
			printOrderItems($book['cover'], $cartItem['quantity'], $book['title'], $book['price']);
			$sum +=$book['price']*$cartItem['quantity'];
		}
		if($isInstock){
			echo"  	<tr class='sumrow'>";
			echo"			<td> sum: " . $sum . " kr </td>";
  		echo"		</tr>";
  	}
		echo"	</tbody>";
	}

	function printOrderItems($img, $qty, $title, $price){
	  echo"	<tr class='cartItem'>";
	  echo"  	<td><img src=$img class='photo'></td>";
	  echo"  	<td>$qty</td>";
	  echo"  	<td>$title</td>";
	  echo"  	<td>$price kr</td>";
	  echo"	</tr>";
	}



 ?>
<?php

require_once('connection.php');  // TODO: use db.php
require_once('util.php');

function getCustomerValues($custID){
	$db = connect();
  //*
	if(isset($_SESSION['user_id'])){ //Not needed if we check which id we send in to prints. 
		$stm = $db->prepare("SELECT * FROM Customers WHERE custID = :sessid");
		$stm->execute([':sessid' => $_SESSION['user_id']]);
	}
	else{
		$stm = $db->prepare("SELECT * FROM Customers WHERE custID = :cookieid");
		$stm->execute([':cookieid' => $custID]);
	}
  //*/
  //$stm = $db->prepare("SELECT * FROM Customers WHERE custID = :id");
  //$stm->execute([':id' => getUserId()]);
	$customer = $stm->fetch(PDO::FETCH_ASSOC); //returns false if there were no matches
	if(isEmpty($customer)){  //Not sure if nessecary?
		$customer = array("fName" => "", "lName" => "", "address" => "", "email" => "");
	}
	return $customer;
}

function isEmpty($customer){
	if($customer['fName'] === NULL){
		return true;
	}else{
		return false;
	}
}

function printRegistrationForm($custID){
	$customer = getCustomerValues($custID);
	echo"		<div class='registerForm'>";
	echo"		<form action='util/register.php' method='POST'>";
	echo"			<h3>User info</h3>";
	echo"				<label for='uname'>Username:</label><br>";
	echo"				<input type='text' id='uname' name='uname'/><br>";
	echo"				<label for='pswrd'>Password:</label><br>";
	echo"				<input type='text' id='pswrd' name='pswrd'/><br>";
	echo"				<label for='fname'>Firstname:</label><br>";
	echo"				<input type='text' id='fname' name='fname' value= '". $customer['fName'] . "'/><br>";
	echo"				<label for='lname'>Lastname:</label><br>";
	echo"				<input type='text' id='lname' name='lname' value= '" . $customer['lName'] . "'/><br>";
	echo"				<label for='addr'>Address:</label><br>";
	echo"				<input type='text' id='addr' name='addr' value='" . $customer['address'] . "'/><br>";
	echo"				<label for='email'>Email:</label><br>";
	echo"				<input type='text' id='email' name='email' value='" . $customer['email'] . "'/><br>";
	echo"				<button type ='submit' class='registerButton'>Register</button>";
	echo"		</form>";
	echo"	</div>";
}

function printCheckoutForm($custID){
	$customer = getCustomerValues($custID);
	echo"		<div class='informationForm'>";
	echo"		<form action='checkout.php' method='GET'>";
	echo"			<h3>Address info</h3>";
	echo"				<label for='fname'>Firstname:</label><br>";
	echo"				<input type='text' id='fname' name='fname' value='" . $customer['fName'] . "'/><br>";
	echo"				<label for='lname'>Lastname:</label><br>";
	echo"				<input type='text' id='lname' name='lname' value='" . $customer['lName'] . "'/><br>";
	echo"				<label for='addr'>Address:</label><br>";
	echo"				<input type='text' id='addr' name='addr' value='" . $customer['address'] . "'/><br>";
	echo"				<label for='email'>Email:</label><br>";
	echo"				<input type='text' id='email' name='email' value='" . $customer['email'] . "'/><br>";
	echo"				<button type ='submit' class='orderButton'>Send order</button>";
	echo"		</form>";
	echo"	</div>";
}

 ?>

<?php 

	session_start();
	require("connection.php");

	function loginUser($username, $password) {
		$customer = loginQuery($username, $password);
		if ($customer['pswrd'] == $password) { //should probably use something like verify_password here
			$_SESSION['user_id'] = $customer['custID'];
			header("Location: ../index.php");
		} else {
			$_SESSION['loginerror'] = 'Login failed, please try again with a different username and/or password.<br><br>';
			header("Location: ../loginform.php");
		}
		exit;
	}

	function loginQuery($username, $password) {
			$db = connect();
			$stm = $db->prepare("SELECT * FROM Passwords WHERE uName = :uname");
			$stm->execute([':uname' => $username]);
			$customer = $stm->fetch(PDO::FETCH_ASSOC); //returns false if there were no matches
			return $customer;
	}

	loginUser($_POST['uname'], $_POST['pswrd']);

?>

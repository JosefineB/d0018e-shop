<?php 
  require_once('db.php');
  require_once("util/search.php"); 

	function findCart($userID){
		//$db = connect();
    global $db;
		if(isset($_SESSION['user_id'])){
			$stm = $db->prepare("SELECT * FROM cartItems WHERE custID = :userID OR custID = :sessID");
			$stm->execute([':userID' => $userID, ':sessID' => $_SESSION['user_id']]);
		}
		else{
			$stm = $db->prepare("SELECT * FROM cartItems WHERE custID = :userID");
			$stm->execute([':userID' => $userID]);
		}
		$cart = $stm->fetchAll();
		$stm = null;
		//printCart($cart, $db);
    return $cart;
	}

	function printCart($cart){
    global $db;
		echo"	<table id='cart'>";
		echo" 	<thead>";
		echo"    <tr>";
		echo"    	<th class='photoCol'>Photo</th>";
		echo"      <th class='qtyCol'>Qty</th>";
		echo"      <th class='stockCol'>Stock</th>";
		echo"      <th class='titleCol'>Product</th>";
		echo"      <th class='priceCol'>Price</th>";
		echo"    </tr>";
		echo"  </thead>";
		echo"  <tbody>";
		$sum=0;
		foreach ($cart as $cartItem) {
			$stm = $db->prepare("SELECT * FROM Books WHERE bookID = :bID");
			$stm->execute([':bID' => $cartItem['bookID']]);
			$book = $stm->fetch(PDO::FETCH_ASSOC); 
			printCartItems($book['cover'], $cartItem['quantity'], $book['title'], $book['price'], $book['stock'], $book['bookID']);
			$sum +=$book['price']*$cartItem['quantity'];
		}
		echo"  	<tr class='checkoutrow'>";
		echo"			<td> sum: " . $sum . " kr </td>";
		echo"			<form action = 'checkoutform.php' method = 'GET'>";
    echo"				<td colspan='5' class='checkout'><button id='submitbtn' type ='submit' >Checkout Now!</button></td>";
    echo"			</form>";
  	echo"		</tr>";
		echo"	</tbody>";
	}

	function printCartItems($img, $qty, $title, $price,$stock, $bookID){
	  echo"	<tr class='cartItem'>";
	  echo"  	<td><img src=$img class='photo'></td>";
	 	echo"		<form action = 'util/updateCart.php' method = 'GET'>";
	  echo"  		<td><input type='text' name = 'quantity' value=$qty class='qtyinput'><button name='bookID' type='submit' value= '" . $bookID . "'>Update</button></td>";
	  echo"		</form>";
	  echo"		<td>$stock</td>";
	  echo"  	<td>$title</td>";
	  echo"  	<td>$price kr</td>";
	  echo"	</tr>";
	}

 ?>

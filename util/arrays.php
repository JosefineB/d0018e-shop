<?php

/* The identity function. */
function identity($x) {
  return $x;
}

/* Returns the identity function if passed null or nothing. */
function defaultToIdentity($f = null) {
  if ($f == null) {
    $f = function ($x) { return $x; };
  }
  return $f;
}

/* Takes a regular array and returns an associative array where each
 * key and value is the result of applying the key and value function to the
 * original value.
 * Maps [val1, val2, ..., valn]
 * to   [kf(val1) => vf(val1), kf(val2) => vf(val2), ..., kf(valn) => vf(valn)].
 */
function array_map_to_assoc($xs, $vf, $kf = null) {
  $kf = defaultToIdentity($kf);
  $tmp = array();
  foreach ($xs as $x) {
    $tmp[$kf($x)] = $vf($x);
  }
  return $tmp;
}

/* Takes an associative array and returns a new associative array where each
 * key and value is the result of applying the key and value function to the
 * original key and value, respectively.
 * Maps [key1 => val1, key2 => val2, ..., keyn => valn]
 * to   [kf(key1) => vf(val1), kf(key2) => vf(val2), ..., kf(keyn) => vf(valn)].
 */
function array_map_assoc($assoc, $vf, $kf = null) {
  $kf = defaultToIdentity($kf);
  $tmp = array();
  foreach ($assoc as $k => $v) {
    $tmp[$kf($k)] = $vf($v);
  }
  return $tmp;
}

/* Like array_map_assoc, but both the key value functions are binary functions
 * that take both the key and the value.
 * Maps [key1 => val1, key2 => val2, ..., keyn => valn]
 * to   [kf(key1, val1) => vf(key1, val1), kf(key2, val2) => vf(key2, val2), ..., kf(keyn, valn) => vf(keyn, valn)].
 */
function array_map_assoc_bi($assoc, $vf, $kf) {
  $tmp = array();
  foreach ($assoc as $k => $v) {
    $tmp[$kf($k, $v)] = $vf($k, $v);
  }
  return $tmp;
}

/* Takes an associative array and returns a new associative array where each
 * key is the result of applying the key function to the original key.
 * Maps [key1 => val1, key2 => val2, ..., keyn => valn]
 * to   [kf(key1) => val1, kf(key2) => val2, ..., kf(keyn) => valn].
 */
function array_map_keys($assoc, $kf) {
  return array_map_assoc($assoc, defaultToIdentity(), $kf);
}

/* Takes an associative array and returns a new associative array where each
 * value is the result of applying the value function to the original value.
 * Maps [key1 => val1, key2 => val2, ..., keyn => valn]
 * to   [key1 => vf(val1), key2 => vf(val2), ..., keyn => vf(valn)].
 */
function array_map_values($assoc, $vf) {
  return array_map_assoc($assoc, $vf);
}

/* Like array_map_keys, but the key function is a binary function that
 * takes both the key and the value (and returns the new key).
 * Maps [key1 => val1, key2 => val2, ..., keyn => valn]
 * to   [kf(key1, val1) => val1, kf(key2, val2) => val2, ..., kf(keyn, valn) => valn].
 */
function array_map_keys_bi($assoc, $kf) {
  return array_map_assoc_bi($assoc, function ($k, $v) { return $v; }, $kf);
}

/* Like array_map_values, but the value function is a binary function that
 * takes both the key and the value (and returns the new value).
 * Maps [key1 => val1, key2 => val2, ..., keyn => valn]
 * to   [key1 => vf(key1, val1), key2 => vf(key2, val2), ..., keyn => vf(keyn, valn)].
 */
function array_map_values_bi($assoc, $vf) {
  return array_map_assoc_bi($assoc, $vf, function ($k, $v) { return $k; });
}

/* Make an associative array from an array of keys and an array of values.
 * Maps ([key1, key2, ..., keyn], [val1, val2, ..., valn])
 * to   [key1 => val1, key2 => val2, ..., keyn => valn]
 */
function array_zip_assoc($keys, $vals) {
  assert(count($keys) == count($vals));
  $len = count($keys);
  $tmp = array();
  for ($i = 0; $i < $len; $i++) {
    $tmp[$keys[$i]] = $vals[$i];
  }
  return $tmp;
}

?>

<?php

require_once('util/connection.php');
require_once('util/util.php');

function getOrders(){
	$db = connect();
	if(userIsLoggedIn()){
		$stm = $db->prepare("SELECT * FROM Orders WHERE custID = ? OR custID = ?");
		$stm->execute([$_COOKIE['custID'], $_SESSION['user_id']]);
	}else{
		$stm = $db->prepare("SELECT * FROM Orders WHERE custID = ?");
		$stm->execute([$_COOKIE['custID']]);
	}
	$orders = $stm->fetchAll();
	return $orders;
}

//Just for testing
function tempOrders(){
	$orders = array();
	$orders[0] = array('orderID' => 1, 'orderDate' => '2019-12-10', 'state' =>1);
	$orders[1] = array('orderID' => 2, 'orderDate' => '2019-12-09', 'state' =>0);
	return $orders;
}

function printOrders($orders){
		echo"	<table id='orderhist'>";
		echo"		<caption>Previous orders</caption>";
		echo" 	<thead>";
		echo"    <tr>";
		echo"    	<th class='ordernr'>Ordernumber</th>";
		echo"      <th class='date'>Orderdate</th>";
		echo"      <th class='status'>Orderstatus</th>";
		echo"    </tr>";
		echo"  </thead>";
		echo"  <tbody>"; 
		foreach ($orders as $order) {
			printOrder($order['orderID'], $order['orderDate'], $order['state']);
		}
		echo"	</tbody>";
}

	function printOrder($ordernr, $date, $status){
	  echo"	<tr class='order'>";
	  echo"  	<td><a href='orderhistory.php?orderID=$ordernr'>$ordernr</a></td>";
	  //echo"		<td>$ordernr</td> ";
	  echo"  	<td>$date</td>";
	  if($status == 1){
	 		echo"  	<td>Active</td>";
	  	echo"		<form action = 'util/handleOrder.php' method = 'GET'>";
	  	echo"  	<td><button name = 'Cancel' type = 'submit' value =' " . $ordernr."'>Cancel</td>";
	  	echo"		</form>";
	  }else{
	  	echo"  	<td>Cancelled</td>";
	  }
	  echo"	</tr>";
	}

 ?>

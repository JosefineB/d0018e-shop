<?php 
	
	require('connection.php');

	function cancelOrder(){
		$db = connect(); 
		$stm = $db->prepare("UPDATE Orders SET state = ? WHERE orderID = ?");
		if (!empty($_GET['Cancel'])) {
			$stm->execute([0, $_GET['Cancel']]);
		}
	}

	cancelOrder();
	header("Location: ../orders.php");
	exit;


 ?>
<?php
  require_once('db.php');
/*
 * Functions related to searching for books in the database.
 */

	function searchDatabase($input){
		$db = connect();
		$stm = $db->prepare("SELECT * FROM Books WHERE title LIKE ? OR genre LIKE ? OR bookID = ?");
		$stm->execute(['%' . $input .'%', '%' . $input .'%', $input]);
		$res1 = $stm->fetchAll();
    $res2 = getBooksByAuthor($input);
    $books = mergeSearchResults($res1, $res2);
		return $books;
	}

  function mergeSearchResults($res1, $res2){
    return array_unique(array_merge($res1,$res2), SORT_REGULAR);
  }

	function search() {
		searchFor($_GET['text']);
	}

	function searchFor($text) {
		$books = searchDatabase($text);
		if (sizeof($books) == 0) {
		  echo "<h3>Sorry, no matches were found</h3>";
		  return;
		}
		printAllMatches($books);
}

  function printAllMatches($books) {
    echo "<div id='p-flex'>'\n";
		foreach ($books as $book){
			if(isset($book['title'])){ 
        $bookId = $book['bookID'];
        $book = getCompleteBookInfo($book);
				printSearchResult($book['cover'], $book['title'], $book['price'], $book['authors'], $bookId, $book['avgRating']); 
			}
		}
    echo "</div>\n";
  }

  function getBooksByAuthor($input){
    $db = connect();
    $stm = $db->prepare("DROP TABLE IF EXISTS Temp");
    $stm->execute();
    $stm = $db->prepare("CREATE TEMPORARY TABLE Temp (SELECT authorID FROM Authors AS authorID WHERE fullname LIKE ?)");
    $stm->execute(['%' . $input . '%']);
    //$stm = $db->prepare("CREATE TEMPORARY TABLE Temp (SELECT authorID FROM Authors AS authorID WHERE fullname LIKE %?%)");
    //$stm->execute([$input]);
    $stm = $db->prepare("DROP TABLE IF EXISTS IDTemp");
    $stm->execute();
    $stm = $db->prepare("CREATE TEMPORARY TABLE IDTemp (SELECT DISTINCT BookAuthors.bookID FROM BookAuthors INNER JOIN Temp ON BookAuthors.authorID = Temp.authorID)");
    $stm->execute();
    $stm = $db ->prepare("SELECT * FROM Books INNER JOIN IDTemp ON Books.bookID=IDTemp.bookID");
    $stm->execute();
    $books = $stm->fetchAll();
    return $books;
  }

	function printSearchResult($img, $name, $price, $author, $id, $avgRating) { 
    $bookKey = REVIEWS_BOOK_COL;
    echo "  <div class='p-flex'>\n";
    echo "    <div class='p-flex-in'>\n";
    echo "      <img class='p-img' src='" . $img . "'/>\n";
    echo "      <div class='p-name'>" . $name . "</div>\n";
    echo "      <div class='p-name'><a href='review.php?$bookKey=$id'>Rating: $avgRating</a></div>\n";
    echo "      <div class='p-price'>" . $price . " kr</div>\n";
    echo "      <div class='p-desc'>" . $author ."</div>\n";
    echo "			<form action = 'util/addtocart.php' method = 'GET'>";
    echo "      	<button class='p-add' type='submit' name='bookID' value='" . $id . "'>Add to Cart</button>\n";
    echo "			</form>";
    echo "    </div>\n";
    echo "  </div>\n";
	}	
?>

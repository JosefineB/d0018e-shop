<?php 
require('header.php');
require('util/prefillForms.php');
?>
<!DOCTYPE html>
<html>
<body>
	<div class='main'>
		<?php 
			if (isset($_SESSION['registererror'])) {
  			echo $_SESSION['registererror'];
  			unset($_SESSION['registererror']);
			}
			printRegistrationForm($_COOKIE['custID']);
		 ?>
	</div>
</body>
</html>

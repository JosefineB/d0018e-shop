<?php
// Takes a book ID that will be reviewed/rated.
// If a review/rating already exists for the book and currently logged-in user,
// the form is pre-populated with the latest info, and the user can then update
// the current record.

//session_start();

require_once('header.php');
require_once('util/db.php');
require_once('util/util.php');

$userId = getUserId();
$bookKey = REVIEWS_BOOK_COL;
$userKey = REVIEWS_USER_COL;
$textKey = REVIEWS_REVIEW_COL;
$ratingKey = REVIEWS_RATING_COL;
$bookId = $_GET[$bookKey];

function getCurrentReview($book, $user) {
  global $textKey;
  global $ratingKey;
  // Take from DB if it exists. Otherwise use defaults.
  $review = getBookReview($book, $user);
  if ($review) {
    return $review;
  } else {
    return array($textKey => '', $ratingKey => 3);
  }
}

$review = getCurrentReview($bookId, $userId);

function checked($rating) {
  global $review;
  global $ratingKey;
  if ($review[$ratingKey] == $rating) {
    return "checked";
  } else {
    return "";
  }
}

function radioButton($number) {
  global $ratingKey;
  $checked = checked($number);
  return "<input type='radio' name='$ratingKey' value='$number' $checked >$number</input>";
}

function printReview($review) {
  global $ratingKey;
  global $textKey;
  $rating = $review[$ratingKey];
  $text = $review[$textKey];
  echo "<p>\n";
  echo "  <strong>Rating:</strong> $rating\n";
  echo "  <br />\n";
  echo "  <strong>Review:</strong> $text\n";
  echo "</p>\n";
}

function printAllReviews($bookId) {
  $reviews = getBookReviews($bookId);
  foreach ($reviews as $review) {
    printReview($review);
  }
}

function printForm() {
  global $bookKey;
  global $userKey;
  global $bookId;
  global $userId;
  global $textKey;
  global $text;
  echo "  <form action='submit_review.php' method='POST'>\n";
  echo "    <input type='hidden' name='$bookKey' value='$bookId' />\n";
  echo "    <input type='hidden' name='$userKey' value='$userId' />\n";
  echo "    Rating:\n";
  echo "    <br />\n";
  echo "    " . radioButton(1) . "\n";
  echo "    " . radioButton(2) . "\n";
  echo "    " . radioButton(3) . "\n";
  echo "    " . radioButton(4) . "\n";
  echo "    " . radioButton(5) . "\n";
  echo "    <br />\n";
  echo "    Review text:\n";
  echo "    <br />\n";
  echo "    <textarea name='$textKey' maxlength='200' rows='5' cols='40'>$text</textarea>\n";
  echo "    <br />\n";
  echo "    <input type='submit' value='Submit' />\n";
  echo "  </form>\n";
}

function printWarning() {
  echo "<h3>You must be <a href='loginform.php'>logged in</a> to review a book.</h3>\n";
}

function printBookInfo($bookId) {
  $book = getCompleteBookInfoById($bookId);
  $bookId = $book['bookID'];
  $avgRating = $book['avgRating'];
  $authors = $book['authors'];
  $img = $book['cover'];
  $title = $book['title'];
  $price = $book['price'];
  echo "  <div class='p-flex'>\n";
  echo "    <div class='p-flex-in'>\n";
  echo "      <img class='p-img' src='" . $img . "'/>\n";
  echo "      <div class='p-name'>" . $title . "</div>\n";
  echo "      <div class='p-name'><a href='review.php?bookID=$bookId'>Rating: $avgRating</a></div>\n";
  echo "      <div class='p-price'>" . $price . " kr</div>\n";
  echo "      <div class='p-desc'>" . $authors ."</div>\n";
  echo "    </div>\n";
  echo "  </div>\n";
}

$text = $review[$textKey];

echo "<html>\n";
echo "<body>\n";
echo "  <div class='main'>\n";
printBookInfo($bookId);
printAllReviews($bookId);
if (userIsLoggedIn()) {
  printForm();
} else {
  printWarning();
}
echo "</div>\n";
echo "</body>\n";
echo "</html>\n";
?>
